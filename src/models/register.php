<?php
namespace register;

require_once('__init__.php');
require_once('models/constraints.php');

use Exception;
use constraints;


/*
 * Exceptions: Errors and Info
 */

class UserRegistryException extends Exception {}
class UserRegistryLogicException extends UserRegistryException {}
class UserRegistryVerficationFail extends UserRegistryException {}


/*
 * UserRegistry
 *
 * The main class. It handles registration requests.
 */

class UserRegistry {

  // This is the field in the registration response used to convey errors that
  // occurred during the verfication.
  const SERVER_ERROR_FIELD_NAME = "_servererror";

  // These are the known form fields used in registration request/response
  const PROFILETRAIT_USERNAME = "username";
  const PROFILETRAIT_PASSWORD = "password";

  // as long as there is no field hierarchy...
  const FIELDCONSTRAINT_LENGTH = "length";
  const FIELDCONSTRAINT_CHARDIV = "diversity";
  const FIELDCONSTRAINT_INVCHAR = "invchar";

  // character groups that have to be present in a password
  protected $mandatoryCharGroups = [
    "Kleinbuchstaben" => "abcdefghijklmnopqrstuvxyz",
    "GrossBuchstaben" => "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
    "Zahlen" => "1234567890",
    "Sonderzeichen" => "$%&",
  ];

  // min/max ranges for fields
  protected $minMaxLengths = [
    UserRegistry::PROFILETRAIT_USERNAME => ['min' => 3, 'max' => 20],
    UserRegistry::PROFILETRAIT_PASSWORD => ['min' => 8, 'max' => 50],
  ];

  // map descriptive terms to single-character-match regular-expressions.
  protected $allowedUsernameCharGroups = [
      "letters" => "\p{L}",
      "number" => "\p{N}",
      "hyphen" => "[-]",
      "underscore" => "[_]",
  ];

  // ---

  function __construct() {
    $this->registryTemplate = $this->_getRegistryTemplate();
  }

  /*
   * Get info about the constraints that the registration applies to fields
   *
   * The return value has the same format as the registration itself, but
   * provides a complete structure for all supported fields.
   */
  function getFieldInfo() {
    // yafkm!!! BSe.
    $registryClone = [];
    foreach($this->registryTemplate as $fieldName => $constraints){
      $fieldValue = [];
      foreach($constraints as $cKey => $cValue) {
        $fieldValue[$cKey] = clone $cValue;
      }
      $registryClone[$fieldName] = $fieldValue;
    }
    return $registryClone;
  }

  function registerNewUser($formData) {

    /*
     * Take a form-data array of field-names and values.  Return an array of the
     * those field-name keys if the value failed a constraint.  The value is an
     * array of failed constraints.
     *
     * There is a special server error entry for errors that cannot be
     * associated with a particular field.
     */

    $registrationErrors = [];
    $serverFails = [];

    foreach($formData as $fieldName => $fieldValue) {
      if (!array_key_exists($fieldName, $this->registryTemplate)) {
        $sr = new constraints\ServerConstraint();
        $sr->setError("Unsupported field: $fieldName");
        $serverFails[] = $sr;
        continue;
      }
      $constraintsResponse = [];
      $fieldconstraints = $this->registryTemplate[$fieldName];
      foreach($fieldconstraints as $constraintName => $constraintTemplate) {
        $constraint = clone $constraintTemplate;
        $constraint->verify($fieldValue);
        if ($constraint->isError()) {
          $constraintsResponse[$constraintName] = $constraint;
          if ($constraint->isFatal()) {
            break;
          }
        }
      }
      if ($constraintsResponse) {
        $registrationErrors[$fieldName] = $constraintsResponse;
      }
    }

    if ($serverFails) {
      $registrationErrors[self::SERVER_ERROR_FIELD_NAME] = $serverFails;
    }
    return $registrationErrors;
  }

  // ---

  function _getRegistryTemplate() {

    // username
    $userLength = $this->minMaxLengths[self::PROFILETRAIT_USERNAME];
    $userLengthConstraint = new constraints\LengthLimitsConstraint(
            $userLength['min'], $userLength['max']);
    $userLengthConstraint->setFatal(True);

    $userInvCharConstraint = new constraints\InvalidCharsConstraint(
            $this->allowedUsernameCharGroups);

    $usernameField = [];
    $usernameField[self::FIELDCONSTRAINT_LENGTH] = $userLengthConstraint;
    $usernameField[self::FIELDCONSTRAINT_INVCHAR] = $userInvCharConstraint;

    // password
    $pwLenght = $this->minMaxLengths[self::PROFILETRAIT_PASSWORD];
    $pwLengthConstraint =  new constraints\LengthLimitsConstraint(
            $pwLenght['min'], $pwLenght['max']);
    $pwLengthConstraint->setFatal(True);

    $pwCharDivConstraint = new constraints\CharDiversityConstraint(
            $this->mandatoryCharGroups);

    $passwordField = [];
    $passwordField[self::FIELDCONSTRAINT_LENGTH] = $pwLengthConstraint;
    $passwordField[self::FIELDCONSTRAINT_CHARDIV] = $pwCharDivConstraint;

    // registry template
    $registryTemplate = [];
    $registryTemplate[self::PROFILETRAIT_USERNAME] = $usernameField;
    $registryTemplate[self::PROFILETRAIT_PASSWORD] = $passwordField;

    return $registryTemplate;
  }
}
